import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'


export default function ExpensePiechart({ expenseRecords }){

	console.log(expenseRecords)

	const [name, setName] = useState([]);
	const [amount, setAmount] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	useEffect(() => {

		if(expenseRecords.length > 0){

			setName(expenseRecords.map(element => element.name));
			setAmount(expenseRecords.map(element => element.amount));
			setBgColors(expenseRecords.map(() => `#${colorRandomizer()}`))

		}

	}, [expenseRecords])

	console.log(name);
	console.log(amount);
	console.log(bgColors);

	const data = {
		labels: name,
		datasets: [{
			data: amount,
			backgroundColor: bgColors,
			hoverBackgroundColor: bgColors,
			radius: '70%'

		}]
	}

	return (

		<Pie data={data} />	
	)

}