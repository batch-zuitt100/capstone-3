import { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';


export default function BalanceBarChart({ balanceRecords }){

	// console.log(balanceRecords);

	const [dates, setDates] = useState([]);
	const [balanceTrend, setBalanceTrend] = useState([]);

	const [newBalance, setNewBalance] = useState([]);

	useEffect(() => {

		if(balanceRecords.length > 0){
			setDates(balanceRecords.map(element => moment(element.createdOn).format('LL')))
		}
						
	}, [balanceRecords]);
	

	useEffect(() => {

		let balance = [];

		balanceRecords.forEach(element => {

			if(element.type === 'Income'){
                balance.push(element.amount)
            } else if (element.type === 'Expense'){
                balance.push(-Math.abs(element.amount))
            }

		})

		setNewBalance(balance);
						
	}, [balanceRecords])

	useEffect(() => {
			
			let result = [];
			let counter = 0;

			newBalance.forEach(element => {				
				result.push(counter += element)
			})

			setBalanceTrend(result)
		
	}, [newBalance])


	const data = {
		labels: dates,
		datasets: [{
			label: 'Balance',
			fill: true,
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255, 99, 132, 1)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: balanceTrend
		}]
	}

	return(
		<Line data={data}/>
	)
}