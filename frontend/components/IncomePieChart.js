import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'


export default function IncomePiechart({ incomeRecords }){

	console.log(incomeRecords)

	const [name, setName] = useState([]);
	const [amount, setAmount] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	useEffect(() => {

		if(incomeRecords.length > 0){

			setName(incomeRecords.map(element => element.name));
			setAmount(incomeRecords.map(element => element.amount));
			setBgColors(incomeRecords.map(() => `#${colorRandomizer()}`))

		}

	}, [incomeRecords])

	console.log(name);
	console.log(amount);
	console.log(bgColors);

	const data = {
		labels: name,
		datasets: [{
			data: amount,
			backgroundColor: bgColors,
			hoverBackgroundColor: bgColors,
			radius: '70%'

		}]
	}

	return (

		<Pie data={data} />
	
	)

}