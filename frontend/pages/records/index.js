import React, { useState, useEffect, useContext } from 'react';
import { Table, Button, Form, Row, Col, Card, CardGroup } from 'react-bootstrap';
import Head from 'next/head'
import Link from 'next/link';
import AppHelper from '../../app-helper';
import moment from 'moment';


export default function Record() {

    const [result, setResult] = useState('')
    const [searchWord, setSearchWord] = useState('')
    const [type, setType] = useState('All');
    // const [balance, setBalance] = useState(result);


    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            console.log(data.records)

            let expenses = [];
            let income = [];
            let balance = [];

            const recordList = data.records.map(record => {

                if(type === 'All'){
                    if(record.description.toLowerCase().includes(searchWord)){

                        if(record.type === 'Income'){
                            balance.push(record.amount)
                        } else if (record.type === 'Expense'){
                            balance.push(-Math.abs(record.amount))
                        }

                        return (

                            record.type === 'Income'
                            ?
                                <CardGroup key={record._id} className="my-3 col-lg-12">
                                    <Card className="border-right-0">
                                        <Card.Body>
                                            <Card.Title>{record.description}</Card.Title>
                                            <Card.Text>
                                                <p className="text-success">{record.type} ({record.name})</p>                                               
                                                {moment(record.createdOn).format('LL')}

                                            </Card.Text>     
                                        </Card.Body>
                                    </Card>
                                    <Card className="border-left-0">
                                        <Card.Body>
                                            <Card.Text className="text-right">
                                                <br />
                                                <h5 className="text-success"> + {record.amount}</h5>
                                                <br />
                                                <p className="text-primary">
                                                   {balance.reduce((accumulator, currentValue) => {
                                                    return (accumulator + currentValue)
                                                  }, 0)} 
                                                </p> 
                                            </Card.Text>
                                        </Card.Body> 
                                    </Card>
                                </CardGroup> 

                                :

                                <CardGroup key={record._id} className="my-3 col-lg-12">
                                    <Card className="border-right-0">
                                        <Card.Body>
                                            <Card.Title>{record.description}</Card.Title>
                                            <Card.Text>
                                                <p className="text-danger">{record.type} ({record.name})</p>
                                                {moment(record.createdOn).format('LL')}

                                            </Card.Text>     
                                        </Card.Body>
                                    </Card>
                                    <Card className="border-left-0">
                                        <Card.Body>
                                            <Card.Text className="text-right">
                                                <br />
                                                <h5 className="text-danger"> - {record.amount}</h5>
                                                <br />
                                                <p className="text-primary">
                                                   {balance.reduce((accumulator, currentValue) => {
                                                    return (accumulator + currentValue)
                                                  }, 0)} 
                                                </p> 
                                            </Card.Text>
                                        </Card.Body> 
                                    </Card>
                                </CardGroup> 
                            )
                    }
                } else if (type === 'Income'){
                    if(record.type === 'Income'){
                        if(record.description.toLowerCase().includes(searchWord)){

                            income.push(record.amount)
                            console.log(income)

                            return (
                                <CardGroup key={record._id} className="my-3">
                                    <Card className="border-right-0">
                                        <Card.Body>
                                            <Card.Title>{record.description}</Card.Title>
                                            <Card.Text>
                                                <p className="text-success">{record.type} ({record.name})</p>
                                                {moment(record.createdOn).format('LL')}

                                            </Card.Text>     
                                        </Card.Body>
                                    </Card>
                                    <Card className="border-left-0">
                                        <Card.Body>
                                            <Card.Text className="text-right">
                                                <br />
                                                <h5 className="text-success"> + {record.amount}</h5>
                                                <br />
                                                <p className="text-primary">
                                                   {income.reduce((accumulator, currentValue) => {
                                                    return (accumulator + currentValue)
                                                  }, 0)} 
                                                </p> 
                                            </Card.Text>
                                        </Card.Body> 
                                    </Card>
                                </CardGroup> 
                            )
                        }
                    }
                
                }else if (type === 'Expense'){
                    if(record.type === 'Expense'){
                        if(record.description.toLowerCase().includes(searchWord)){

                            expenses.push(record.amount)
                            console.log(expenses)

                            return (
                                <CardGroup key={record._id} className="my-3">
                                    <Card className="border-right-0">
                                        <Card.Body>
                                            <Card.Title>{record.description}</Card.Title>
                                            <Card.Text>
                                                <p className="text-danger">{record.type} ({record.name})</p>
                                                {moment(record.createdOn).format('LL')}

                                            </Card.Text>     
                                        </Card.Body>
                                    </Card>
                                    <Card className="border-left-0">
                                        <Card.Body>
                                            <Card.Text className="text-right">
                                                <br />
                                                <h5 className="text-danger"> - {record.amount}</h5>
                                                <br />
                                                <p className="text-primary">
                                                   {expenses.reduce((accumulator, currentValue) => {
                                                    return (accumulator + currentValue)
                                                  }, 0)} 
                                                </p> 
                                            </Card.Text>
                                        </Card.Body> 
                                    </Card>
                                </CardGroup> 
                            )
                        }
                    }
                }

            })
            
            setResult(recordList.reverse())
        })

    }, [type, searchWord])
  

    return (
        <React.Fragment>
            <Head>
                <title>Records</title>
            </Head>

            <h1 className="py-4">Records</h1>
            <Link href="/records/add">
                <Button className="mb-2" variant="success">
                    Add
                </Button>                
            </Link>
            <Row>
                <Col>
                    <Form>
                        <Form.Group controlId="type">
                            <Form.Control 
                                as="select" 
                                onChange={ (e) => setType(e.target.value) }>
                                    <option value="All">All</option>
                                    <option value="Income">Income</option>
                                    <option value="Expense">Expense</option>
                            </Form.Control>
                        </Form.Group>
                    </Form>
                </Col>
                <Col>
                    <Form>
                        <Form.Group>
                            <Form.Control
                                type="text"
                                value={searchWord}
                                placeholder="Search for record"
                                onChange={e => setSearchWord(e.target.value.toLowerCase())}
                            />
                        </Form.Group>               
                    </Form>
                </Col>
            </Row>

            <div className="mb-3">
                    {result}
            </div>
       
        </React.Fragment>
            
    )
}

