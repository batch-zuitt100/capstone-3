import React, { useState, useEffect, useContext } from 'react';
import { Table, Button } from 'react-bootstrap';
import Head from 'next/head'
import Link from 'next/link';
import AppHelper from '../../app-helper';


export default function Category({ data }) {

    const [categories, setCategories] = useState([])

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        // console.log(localStorage.getItem('token'))
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            console.log(data.categories)

            const categoryList = data.categories.map(category => {
                return (
                    <tr key={category.name}>
                        <td>{category.name}</td>
                        <td>{category.type}</td>
                    </tr>
                )
            })

            setCategories(categoryList)        
        })
    }, [])

    // console.log(categories)


    return (
        <React.Fragment>
                <Head>
                    <title>Categories</title>
                </Head>

                <h1 className="py-4">Categories</h1>
                <Link href="/categories/add">
                    <Button className="mb-2" variant="success">
                        Add
                    </Button>                
                </Link>
                
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Category Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        { categories }
                    </tbody>
                </Table>
            </React.Fragment>
    )
}

