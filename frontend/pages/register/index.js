import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function index() {
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === false){
                fetch('http://localhost:4000/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        setFirstName('');
                        setLastName('');
                        setMobileNo('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                          icon: 'success',
                          title: 'Thank you for registering!',
                          showConfirmButton: false,
                          timer: 1500
                        })

                        Router.push('/login');
                    }
                })
            }
        })

        // console.log('Thank you for registering!');
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
            },[email, password1, password2]);

        return (
            <React.Fragment>
                <h4 className="text-center pt-3">Create an account</h4>
                <Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-3">
                    <Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="firstName"
                            placeholder="First Name"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="lastName"
                            placeholder="Last Name"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Mobile No.</Form.Label>
                        <Form.Control 
                            type="text"
                            placeholder="Mobile No."
                            value={mobileNo}
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control 
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Password"
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Verify Password"
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                            required
                        />
                    </Form.Group>

                    {isActive
                        ?
                        <Button 
                            variant="primary"
                            type="submit"
                            id="submitBtn"
                        >
                            Register
                        </Button>
                        :
                        <Button 
                            variant="primary"
                            type="submit"
                            id="submitBtn"
                            disabled
                        >
                            Register
                        </Button>
                    }
            
                </Form>
            </React.Fragment>
        )
};

