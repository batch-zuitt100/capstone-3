import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Container } from 'react-bootstrap';
import Head from 'next/head';
import IncomePieChart from '../../components/IncomePieChart';
import ExpensePieChart from '../../components/ExpensePieChart';
import moment from 'moment';

import AppHelper from '../../app-helper';


export default function Record() {

    const [records, setRecords] = useState([]);
    const [incomeRecords, setIncomeRecords] = useState([]);
    const [expenseRecords, setExpenseRecords] = useState([]);

    const [dateStart, setDateStart] = useState(new Date());
    const [dateEnd, setDateEnd] = useState(new Date());

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            // console.log(data.records)

            setRecords(data.records) 

        })

    }, [])

    useEffect(() => {

        let income = [];
        let expense = [];

        records.forEach(element => {

            if (moment(element.createdOn).format('L') >= moment(dateStart).format('L') && moment(element.createdOn).format('L') <= moment(dateEnd).format('L')) {

                if(element.type === 'Income'){
                    income.push(element)
                } else if (element.type === 'Expense'){
                    expense.push(element)
                }

            }       
        })

        console.log(income)

        setIncomeRecords(income)
        setExpenseRecords(expense)

    }, [dateStart, dateEnd])



    return (
        <React.Fragment>
            <Head>
                <title>Category Breakdown</title>
            </Head>
            <h2 className="py-4">Category Breakdown</h2>
             <Row>
                <Col>
                    <Form>
                        <Form.Group>
                            <Form.Label>From</Form.Label>
                            <Form.Control 
                                type="date"
                                value={dateStart}
                                onChange={e => setDateStart(e.target.value)}
                            />
                        </Form.Group>
                    </Form>
                </Col>
                <Col>
                    <Form>
                        <Form.Group>
                            <Form.Label>To</Form.Label>
                            <Form.Control 
                                type="date"
                                value={dateEnd}
                                onChange={e => setDateEnd(e.target.value)}
                            />
                        </Form.Group>               
                    </Form>
                </Col>
            </Row>
            <Container>
                <div className="row">
                    <div className="col-6">
                        <h3>Income Breakdown</h3>
                        <IncomePieChart incomeRecords={incomeRecords} className=''/>
                    </div>
                    <div className="col-6">
                        <h3>Expenses Breakdown</h3>
                        <ExpensePieChart expenseRecords={expenseRecords}/>
                    </div>
                </div>
            </Container>  
        </React.Fragment>
        
    )
}

