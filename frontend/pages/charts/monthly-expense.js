import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import ExpenseBarChart from '../../components/ExpenseBarChart';
import AppHelper from '../../app-helper';


export default function Record() {

    const [records, setRecords] = useState([]);
    const [expenseRecords, setExpenseRecords] = useState([]);

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            // console.log(data.records)

            setRecords(data.records) 

        })

    }, [])

    useEffect(() => {

        let expenses = []

        records.forEach(element => {

            if(element.type === "Expense"){
                expenses.push(element)
            }

        })

        console.log(expenses)

        setExpenseRecords(expenses)

    }, [records])



    return (
        <React.Fragment>
            <Head>
                <title>Monthly Expense</title>
            </Head>
            <h2 className="py-4">Monthly Expenses in PHP</h2>
            <ExpenseBarChart expenseRecords={expenseRecords}/>
        </React.Fragment>
        
    )
}

